<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- ** generic template -->
	<xsl:template match="*">
		<xsl:element name="{local-name()}">
			<xsl:copy-of select="@*" />
			<xsl:apply-templates select="*|text()" />
		</xsl:element>
	</xsl:template>
	<xsl:template match="/">
		<root>
			<xsl:apply-templates select="*/div[@class='isotope-container' or @class='isotope-container isotope-child']"/>
		</root>
	</xsl:template>
	<xsl:template match="*/div[@class='isotope-container isotope-child']">
		<group>
			<xsl:analyze-string select="h3" regex="(\w*) ([M|C])\((\d+)\) : (.*)">

				<xsl:matching-substring>
					<xsl:attribute name="name">
						<xsl:value-of select="regex-group(1)"/>
					</xsl:attribute>
					<xsl:attribute name="type">
						<xsl:value-of select="regex-group(2)"/>
					</xsl:attribute>
					<xsl:attribute name="c">
						<xsl:value-of select="regex-group(3)"/>
					</xsl:attribute>
					<xsl:attribute name="label">
						<xsl:value-of select="regex-group(4)"/>
					</xsl:attribute>
				</xsl:matching-substring>

			</xsl:analyze-string>
			<xsl:apply-templates select="*/div[@class='isotope-container']" />
			<xsl:apply-templates select="*/div[@class='isotope-container isotope-child']" />
		</group>
	</xsl:template>
	<xsl:template match="*/div[@class='isotope-container']">
		<segment>
			<xsl:analyze-string select="p[position() = 1]/text()[normalize-space(.)]" regex="([M|C])\(([0-9]+)\).:">

				<xsl:matching-substring>
					<xsl:attribute name="type">
						<xsl:value-of select="regex-group(1)"/>
					</xsl:attribute>
					<xsl:attribute name="occurence">
						<xsl:value-of select="regex-group(2)"/>
					</xsl:attribute>
				</xsl:matching-substring>

			</xsl:analyze-string>
			<xsl:value-of select="p/h3/a"/>
		</segment>
	</xsl:template>
</xsl:stylesheet>