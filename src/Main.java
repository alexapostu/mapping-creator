import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import net.sf.saxon.Transform;

public class Main {
    private static final String TRANSFORM_MAPPING_FILE_NAME = "app/transform.xslt";
    private static final String TRANSFORM_HTML_FILE_NAME    = "app/transformHtml.xslt";
    private static final String MESSAGE_START               = "<div class=\"msg_struct_descr\">";
    private static final String MESSAGE_END                 = "<h3>Message structure</h3>";
    private static final String DUMB_PARAGRAPHS             = "</div></p>";
    private static final String LESS_DUMB_PARAGRAPHS        = "</div>";

    public static void main(String[] args) throws Exception {
        String mappingURL = args[0];
        String fileName = getResultFileName(args[1]);
        String mappingXML = getMessageFromWebpage(mappingURL);
        mappingXML = mappingXML.replaceAll(DUMB_PARAGRAPHS, LESS_DUMB_PARAGRAPHS);
        writeToFile(mappingXML, "temp.xml");
        generateXMLMapping(fileName);
        generateHTMLMapping(fileName);
        cleanupTempFiles();
        // writeToFile(mappingXML, args);

    }

    private static void cleanupTempFiles() {
        (new File("temp.xml")).delete();
    }

    private static String getResultFileName(String fileName) {
        File newFile = new File(concatPaths(System.getProperty("user.dir"), fileName));
        newFile.mkdirs();
        return concatPaths(newFile.getAbsolutePath(), fileName);
    }

    private static void generateXMLMapping(String fileName) throws Exception {
        String[] arglist = { "-o:" + fileName + ".xml", "temp.xml", TRANSFORM_MAPPING_FILE_NAME };

        Transform.main(arglist);
    }

    private static void generateHTMLMapping(String fileName) throws Exception {
        String[] arglist = { "-o:" + fileName + ".html", fileName + ".xml", TRANSFORM_HTML_FILE_NAME };

        Transform.main(arglist);
    }

    private static void writeToFile(String mappingXML, String fileName) {
        BufferedWriter writer = null;
        try {
            File logFile = new File(fileName);
            writer = new BufferedWriter(new FileWriter(logFile));
            writer.write(mappingXML);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try { // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }

    }

    private static String getMessageFromWebpage(String mappingURL) {
        String mappingXML = null;
        URL url;
        InputStream is = null;
        BufferedReader br;
        String line;

        try {
            url = new URL(mappingURL);
            is = url.openStream(); // throws an IOException
            br = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            if (sb.indexOf(MESSAGE_END) != -1) {
                sb.setLength(sb.indexOf(MESSAGE_END));
            }
            mappingXML = sb.substring(sb.indexOf(MESSAGE_START));

        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null)
                    is.close();
            } catch (IOException ioe) {
                // nothing to see here
            }
        }
        return mappingXML;
    }

    public static String concatPaths(String dirPath, String subDirPath) {
        return dirPath.replaceAll("\\/+$|\\\\+$", "") + File.separator
                + subDirPath.replaceAll("^\\/+|^\\\\+", "");
    }

}
